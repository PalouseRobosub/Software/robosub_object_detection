import os
import tensorflow as tf

os.chdir('../../robosub_2022_dataset/') # specify path to dataset directory

print(os.getcwd())
print(tf.__version__)

from tflite_model_maker.config import QuantizationConfig
from tflite_model_maker.config import ExportFormat
from tflite_model_maker import model_spec
from tflite_model_maker import object_detector
from absl import logging

tf.get_logger().setLevel('ERROR')
logging.set_verbosity(logging.ERROR)

spec = model_spec.get('efficientdet_lite0')
train_data, validation_data, test_data = object_detector.DataLoader.from_csv('./res.csv') #.csv file name

model = object_detector.create(train_data, model_spec=spec, batch_size=1, train_whole_model=True, validation_data=validation_data)
model.evaluate(test_data)
model.export(export_dir='.')