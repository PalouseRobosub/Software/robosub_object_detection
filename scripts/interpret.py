import numpy as np
from PIL import Image
from PIL import ImageDraw
from PIL import ImageColor
from PIL import ImageFont
import tensorflow as tf
import matplotlib.pyplot as plt

def display_image(image):
    fig = plt.figure(figsize=(20, 15))
    plt.grid(False)
    plt.imshow(image)
    plt.show()

def draw_bounding_box(image,
                      ymin,
                      xmin,
                      ymax,
                      xmax,
                      color,
                      label,
                      font,
                      thickness=4):
    """Adds a bounding box to an image."""
    draw = ImageDraw.Draw(image)
    im_width, im_height = image.size
    (left, right, top, bottom) = (xmin * im_width, xmax * im_width,
                                  ymin * im_height, ymax * im_height)
    draw.line([(left, top), (left, bottom), (right, bottom), (right, top),
               (left, top)],
              width=thickness,
              fill=color)

    # If the height of the label strings added to the top of the bounding
    # box exceeds the top of the image, add it below the bounding box
    # instead of above.
    label_height = font.getsize(label)[1]
    # label has a top and bottom margin of 0.05x.
    total_label_height = (1 + 2 * 0.05) * label_height

    if top > total_label_height:
        text_bottom = top
    else:
        text_bottom = top + total_label_height

    text_width, text_height = font.getsize(label)
    margin = np.ceil(0.05 * text_height)
    draw.rectangle([(left, text_bottom - text_height - 2 * margin),
                    (left + text_width, text_bottom)],
                    fill=color)
    draw.text((left + margin, text_bottom - text_height - margin),
              label,
              fill="black",
              font=font)

if __name__=='__main__':
    liteModel = "../models/model.tflite"

    # Load the TFLite model and allocate tensors.
    interpreter = tf.lite.Interpreter(model_path=liteModel)
    interpreter.allocate_tensors()

    # Get input and output tensors.
    input_details = interpreter.get_input_details()
    output_details = interpreter.get_output_details()

    input_shape = input_details[0]['shape']
    width = input_shape[2]
    height = input_shape[1]

    img = Image.open("../Images/toilets.jpg").resize((width, height))

    input_data = np.array(np.expand_dims(img, axis=0), dtype=np.float32)
    interpreter.set_tensor(input_details[0]['index'], input_data)

    interpreter.invoke()

    locations = interpreter.get_tensor(output_details[0]['index'])
    classes = interpreter.get_tensor(output_details[1]['index'])
    scores = interpreter.get_tensor(output_details[2]['index'])
    targets = interpreter.get_tensor(output_details[3]['index'])

    labelPath = "../labels/labels.txt"
    labelFile = open(labelPath)

    labels = labelFile.readlines()

    labelFile.close()

    labels = list(map(lambda s: s.strip(), labels))
    for i in range(0, int(targets[0])):
        print(labels[int(classes[0][i])], "   ", scores[0][i])
        ymin, xmin, ymax, xmax = tuple(locations[0][i])
        displayColor = ImageColor.getrgb('yellow')
        displayLabel = labels[int(classes[0][i])] + "  " + str(scores[0][i])
        displayFont = ImageFont.load_default()
        draw_bounding_box(img, ymin, xmin, ymax, xmax, displayColor,
                          displayLabel, displayFont)
        i += 1

    display_image(img)
