#This script counts and displays the number of images per class

from os import listdir
import xml.etree.ElementTree as ET

#Returns a set of classes in the xml file
def get_classes_from_xml(xml_file):
    tree = ET.parse(xml_file)
    root = tree.getroot()

    target_set = set()

    for label in root.iter('object'):
        target_name = label.find("name").text
        target_set.add(target_name)

    return target_set

if __name__ == "__main__":
    #Gets a list of xml files in the directory
    files = listdir("../images/")
    files = filter(lambda file_name: file_name.endswith('.xml'), files)
    files = list(files)

    #Creates dictionary of the classes based on what is in the labels file
    target_dictionary = {}
    with open("../labels.txt") as f:
        labels = f.readlines()
        labels = list(map(lambda s: s.strip(), labels))
        for label in labels:
            target_dictionary[label] = 0

    #Gets what classes are in each xml file and adding their count to the dictionary
    for xml_file in files:
        targets = get_classes_from_xml("../images/" + xml_file)
        for target in targets:
            target_dictionary[target] += 1
    
    #Prints the count for each target
    for target in target_dictionary:
        print(target + ": ", target_dictionary[target])
