import argparse
import cv2
import os

#code modified from https://www.geeksforgeeks.org/changing-the-contrast-and-brightness-of-an-image-using-python-opencv/

def SetBrightnessContrast(img, brightness=255, contrast=127):
    brightness = int((brightness - 0) * (255 - (-255)) / (510 - 0) + (-255))

    contrast = int((contrast - 0) * (127 - (-127)) / (254 - 0) + (-127))

    if brightness != 0:
        if brightness > 0:
            shadow = brightness
            max = 255

        else:
            shadow = 0
            max = 255 + brightness

        al_pha = (max - shadow) / 255
        ga_mma = shadow

        # The function addWeighted
        # calculates the weighted sum
        # of two arrays
        cal = cv2.addWeighted(img, al_pha,
                              img, 0, ga_mma)

    else:
        cal = img

    if contrast != 0:
        Alpha = float(131 * (contrast + 127)) / (127 * (131 - contrast))
        Gamma = 127 * (1 - Alpha)

        # The function addWeighted calculates
        # the weighted sum of two arrays
        cal = cv2.addWeighted(cal, Alpha,
                              cal, 0, Gamma)
    return cal

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("dataset_path", type=str, help="""specifies a path to                           a dataset with images to shade.""")
    args = parser.parse_args()

    files = os.listdir(args.dataset_path)
    files = filter(lambda file_name: file_name.endswith('.jpg'), files)
    files = list(files)

    for file_name in files:
        file_prefix = file_name[:-4] 
        original = cv2.imread(os.path.join(args.dataset_path, file_name))
        image1 = SetBrightnessContrast(original, 200, 150)
        image2 = SetBrightnessContrast(original, 300, 75)
        image3 = SetBrightnessContrast(original, 100, 100)
        image4 = SetBrightnessContrast(original, 300, 200)
        file1 = file_prefix + "_b_down_c_up.jpg"
        file2 = file_prefix + "_b_up_c_down.jpg"
        file3 = file_prefix + "_b_down_c_down.jpg"
        file4 = file_prefix + "_b_up_c_up.jpg"
        
        cv2.imwrite(os.path.join(args.dataset_path, file1),image1)
        cv2.imwrite(os.path.join(args.dataset_path, file2),image2)
        cv2.imwrite(os.path.join(args.dataset_path, file3),image3)
        cv2.imwrite(os.path.join(args.dataset_path, file4),image4)

