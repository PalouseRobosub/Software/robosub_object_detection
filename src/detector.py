import numpy as np
from PIL import Image
from PIL import ImageDraw
from PIL import ImageColor
from PIL import ImageFont
import tensorflow as tf
import matplotlib.pyplot as plt
import rclpy
from rclpy.node import Node 
from sensor_msgs.msg import Image as Image_msg
import cv2
from cv_bridge import CvBridge
import argparse

class Detector(Node):
    def __init__(self):
        super().__init__('object_detector')
        parser = argparse.ArgumentParser()
        parser.add_argument('model',  nargs=1, type=str, help="The path to the model file and the file name")
        parser.add_argument('labels',  nargs=1, type=str, help="The path to the labels file and the file name")
        args = parser.parse_args()
        liteModel = args.model
        labels = args.labels
        
        delegate = tf.lite.experimental.load_delegate('libedgetpu.so.1')

        # Load the TFLite model and allocate tensors.
        self.interpreter = tf.lite.Interpreter(model_path=liteModel[0],
                                               experimental_delegates = [delegate])
        self.interpreter.allocate_tensors()

        labelPath = labels[0]
        labelFile = open(labelPath)

        labels = labelFile.readlines()

        labelFile.close()

        self.labels = list(map(lambda s: s.strip(), labels))

        self.sub = self.create_subscription(Image_msg, "/vision/left/raw", 
                                            self.imageCallback, 1)
        self.pub = self.create_publisher(Image_msg, "/vision/left/pretty", 1)

    def draw_bounding_box(self, 
                          image,
                          ymin,
                          xmin,
                          ymax,
                          xmax,
                          color,
                          label,
                          font,
                          thickness=4):
        """Adds a bounding box to an image."""
        draw = ImageDraw.Draw(image)
        im_width, im_height = image.size
        (left, right, top, bottom) = (xmin * im_width, xmax * im_width,
                                      ymin * im_height, ymax * im_height)
        draw.line([(left, top), (left, bottom), (right, bottom), (right, top),
                   (left, top)],
                  width=thickness,
                  fill=color)

        # If the height of the label strings added to the top of the bounding
        # box exceeds the top of the image, add it below the bounding box
        # instead of above.
        label_height = font.getsize(label)[1]
        # label has a top and bottom margin of 0.05x.
        total_label_height = (1 + 2 * 0.05) * label_height

        if top > total_label_height:
            text_bottom = top
        else:
            text_bottom = top + total_label_height

        text_width, text_height = font.getsize(label)
        margin = np.ceil(0.05 * text_height)
        draw.rectangle([(left, text_bottom - text_height - 2 * margin),
                        (left + text_width, text_bottom)],
                        fill=color)
        draw.text((left + margin, text_bottom - text_height - margin),
                  label,
                  fill="black",
                  font=font)

    def imageCallback(self, msg):
        bridge = CvBridge()

        input_details = self.interpreter.get_input_details()
        output_details = self.interpreter.get_output_details()

        input_shape = input_details[0]['shape']
        width = input_shape[2]
        height = input_shape[1]
        
        img = bridge.imgmsg_to_cv2(msg, desired_encoding='passthrough')
        img2= cv2.resize(img, dsize=(height,width))
        rgb = cv2.cvtColor(img2, cv2.COLOR_BGR2RGB)
        rgbTensor = tf.convert_to_tensor(rgb, dtype=tf.uint8)
        rgbTensor = tf.expand_dims(rgbTensor, 0)

        self.interpreter.set_tensor(input_details[0]['index'], rgbTensor)

        self.interpreter.invoke()

        scores = self.interpreter.get_tensor(output_details[0]['index'])
        locations = self.interpreter.get_tensor(output_details[1]['index'])
        targets = self.interpreter.get_tensor(output_details[2]['index'])
        classes = self.interpreter.get_tensor(output_details[3]['index'])
      
        imgPil = Image.fromarray(rgb)
        for i in range(0, int(targets[0])):
            c = int(classes[0][i])
            score = scores[0][i]
            if (score < 0.3): #arbitrary number
                continue
            try:
                print(self.labels[c], "   ", score)
                ymin, xmin, ymax, xmax = tuple(locations[0][i])
                displayColor = ImageColor.getrgb('yellow')
                displayLabel = (self.labels[int(classes[0][i])] + "  " + 
                                str(scores[0][i]))
                displayFont = ImageFont.truetype(
                         "/usr/share/fonts/truetype/freefont/FreeMono.ttf", 28)
                self.draw_bounding_box(imgPil, ymin, xmin, ymax, xmax, 
                                       displayColor, displayLabel, displayFont)
            except IndexError:
                print(len(self.labels))
                print(c)

            i += 1

        openCvImage = np.array(imgPil)
        openCvImage = cv2.resize(openCvImage,
                                   dsize = (img.shape[1], img.shape[0]))
        image_message = bridge.cv2_to_imgmsg(openCvImage, encoding="rgb8")
        self.pub.publish(image_message)

if __name__ == "__main__":
    rclpy.init(args = None)
    detector = Detector()
    rclpy.spin(detector)
