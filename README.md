# PalouseRobosub Object Detection
This repository contains the Palouse Robosub-specific ROS nodes for employing deep learning neural nets for vision inferences.

## Requirements
The current implementation relies on TensorFlow 2 - this can be installed
through Pip.

## Setup
Place a tensorflow lite model in robosub_object_detection/models/ and name it model.tflite. Place a label map file in corresponding to the model in robosub_object_detection/labels/ and name it labels.txt.
